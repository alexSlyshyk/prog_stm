#!/usr/bin/python3
# encoding: utf-8
'''
prog_stm -- simple help script for flashing stm32 MCU's

@author:     Slyshyk Oleksiy

@copyright:  2015 TMX. All rights reserved.

@license:    license

@contact:    alexSlyshyk@gmail.com
@deffield    updated: 2015-04-25
'''

import sys
import os
import platform
import pathlib
import subprocess

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

__all__ = []
__version__ = 0.1
__date__ = '2015-04-24'
__updated__ = '2015-04-25'

LICENSE = '''%s

  Created by user_name on %s.
  Copyright 2015 organization_name. All rights reserved.

  Licensed under the Apache License 2.0
  http://www.apache.org/licenses/LICENSE-2.0

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
'''

DEBUG = 1
TEST_FILE_CACHE = 0


FLASHERS = ["st-flash", "ST-LINK_CLI.exe"]

FLASER_PATHS = []
if platform.system().find("Linux") >= 0:
    FLASER_PATHS = ["~/bin", "~/projects/bin", "~/projects"]
elif platform.system().find("Windows") >= 0:
    FLASER_PATHS.append("C:\\Program Files (x86)\\STMicroelectronics\\STM32 ST-LINK Utility\\ST-LINK Utility")
    FLASER_PATHS.append("C:\\Program Files\\STMicroelectronics\\STM32 ST-LINK Utility\\ST-LINK Utility")


class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg

    def __str__(self):
        return self.msg

    def __unicode__(self):
        return self.msg


class FileCache:
    def __init__(self):
        self.hist = self.__read_history()
        if self.hist is None:
            self.hist = dict()

    def __config_dir(self):
        home = os.getenv("HOME")
        config = "{}/.config".format(home)
        if os.path.exists(config):
            return config
        return None

    def __config_file_path(self):
        cfg_dir = self.__config_dir()
        if cfg_dir:
            return cfg_dir + "/prog_stm.hist"
        return None

    def __read_history(self):
        fn = self.__config_file_path()
        if fn is None:
            return None

        h = None
        try:
            h = open(fn)
        except OSError as e:
            print(e)
        if h is None:
            return None

    def append(self, x, y):
        self.hist.setdefault(x, y)

        print(self.hist)


def flasher():
    for p in FLASER_PATHS:
        for f in FLASHERS:
            ps = os.path.expanduser(p)
            if(DEBUG):
                print("Search flasher %s in %s" % (f, ps))
            for root, dirs, files in os.walk(ps):
                dirs
                if f in files:
                    return os.path.join(root, f)

    return None


def file_suffix(flasher_path):
    if flasher_path.find("st-flash") >= 0:
        return ".bin"
    if flasher_path.lower().find("st-link_cli") >= 0:
        return ".bin"
    return None


def release_file(work_dir, file_pattern):
    wd = os.path.expanduser(work_dir)
    for root, dirs, files in os.walk(wd):
        dirs
        for file in files:
            if pathlib.PurePath(file).suffix == file_pattern:
                return os.path.join(root, file)
    return ""


def flash(work_dir, start_addr):
    flash_prg = flasher()
    flash_file = release_file(work_dir, file_suffix(flash_prg))
    res = 1
    cmd = [];
    if flash_prg.find("st-flash") >= 0:
        cmd = [flash_prg, "--reset", "write", flash_file, start_addr]
    if flash_prg.lower().find("st-link_cli") >= 0:
        cmd = [flash_prg, "-c", "SWD", "-Q", "-P", flash_file, start_addr, "-Rst"]

    if(DEBUG):
        str_cmd = ""
        for i in cmd:
            str_cmd += i
            str_cmd += " "
        print("CMD:{}".format(str_cmd));
    res = subprocess.call(cmd)

    if res == 0:
        print("Flashing %s OK." % flash_file)
    else:
        print("Flashing %s FAIL." % flash_file)


def test_fc():
    cache = FileCache()
    cache.append("xxxxx", "yyyyy")

    return 0


def main(argv=None):  # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = LICENSE % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument('-V', '--version', action='version', version=program_version_message)
        parser.add_argument('--path', dest='path', action='store', default="")
        parser.add_argument('--addr', dest='addr', action='store', default='0x8000000')

        # Process arguments
        args = parser.parse_args()

        #verbose = args.verbose
        #DEBUG = verbose

        #if verbose > 0:
        #    print("Verbose mode on")

        flash(args.path, args.addr)

    except KeyboardInterrupt:
        #  handle keyboard interrupt ###
        return 0
    except Exception as e:
        if DEBUG:
            raise(e)
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    sys.exit(main())
