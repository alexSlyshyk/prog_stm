#!/usr/bin/python3
# encoding: utf-8

import sys
import os
import pathlib
import shutil

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

__all__ = []
__version__ = 0.1
__date__ = '2015-07-17'
__updated__ = '2015-07-17'


def make_dist(path):
    dist = os.path.join(path, "dist")

    if os.path.exists(dist):
        return dist
    else:
        try:
            os.mkdir(dist)
            return dist
        except Exception as e:
            print(e)
    return None


def file_list(path):
    res = []
    wd = os.path.expanduser(path)
    for root, dirs, files in os.walk(wd):
        dirs
        for file in files:
            pp = pathlib.PurePath(file)
            rr = os.path.join(root, file)
            if pp.suffix == '.bin' or pp.suffix == '.exe' or (pp.suffix == '' and os.access(rr, os.X_OK)):
                if not os.path.isdir(rr) and str(rr).find("git") < 0 and str(rr).find("dist") < 0:
                    if str(rr).find("qtc_arm") > 0 or str(rr).find("qtc_rpi") > 0:
                        res.append(rr)
    return res

if __name__ == '__main__':
    program_name = "RPi deploy"
    program_license = "TODO"
    parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument("-v", "--verbose", dest="verbose", action="count",
                        help="set verbosity level [default: %(default)s]", default=0)
    parser.add_argument('-p', '--path', dest='path', action='store', default=".")
    try:
        # Process arguments
        args = parser.parse_args()

        verbose = args.verbose

        if verbose > 0:
            print("Verbose mode on")

        fl = file_list(args.path)
        if verbose > 0:
            for l in fl:
                print(l)
        #make dir "dist"
        dist = make_dist(args.path)
        if dist:
            for l in fl:
                print("Copy {} to {}".format(l, dist))
                shutil.copy2(l, dist)

    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        sys.exit(0)
    except Exception as e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        sys.exit(2)
